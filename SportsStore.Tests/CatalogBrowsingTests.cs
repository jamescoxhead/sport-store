﻿namespace SportsStore.Tests
{
    using System.Linq;
    using Core.Models.Entities;
    using Core.Models.ViewModels;
    using NUnit.Framework;
    using Ui.Controllers;

    [TestFixture]
    public class CatalogBrowsingTests
    {
        [Test]
        public void CanViewASinglePageOfProducts()
        {
            // Arrange - given 5 products in the repository
            var repository = TestHelpers.MockProductRepository(new Product { Name = "P1" }, new Product { Name = "P2" },
                                                               new Product { Name = "P3" }, new Product { Name = "P4" },
                                                               new Product { Name = "P5" });

            var controller = new ProductsController(repository) { PageSize = 3 };

            // Act - when the user asks for the second page
            var result = controller.List(null, 2);

            // Assert - they'll see the last 2 products
            var viewModel = (ProductsListViewModel) result.ViewData.Model;
            var displayedProducts = viewModel.Products;
            displayedProducts.Count().ShouldEqual(2);
            displayedProducts.ElementAt(0).Name.ShouldEqual("P4");
            displayedProducts.ElementAt(1).Name.ShouldEqual("P5");
        }

        [Test]
        public void CanViewProductsFromAllCategories()
        {
            // Arrange - if 2 products are in 2 different categories
            var repository = TestHelpers.MockProductRepository(new Product { Name = "Artemis", Category = "Greek" },
                                                               new Product { Name = "Neptune", Category = "Roman" });
            var controller = new ProductsController(repository);

            // Act - when we ask for "all products"
            var result = controller.List(null, 1);

            // Assert - we get both products
            var viewModel = (ProductsListViewModel) result.ViewData.Model;
            viewModel.Products.Count.ShouldEqual(2);
            viewModel.Products[0].Name.ShouldEqual("Artemis");
            viewModel.Products[1].Name.ShouldEqual("Neptune");
        }

        [Test]
        public void CanViewProductsFromSingleCategory()
        {
            // Arrange - if 2 products are in 2 different categories
            var repository = TestHelpers.MockProductRepository(new Product { Name = "Artemis", Category = "Greek" },
                                                               new Product { Name = "Neptune", Category = "Roman" });
            var controller = new ProductsController(repository);

            // Act - when we ask for a single category
            var result = controller.List("Roman", 1);

            // Assert - we get both products
            var viewModel = (ProductsListViewModel) result.ViewData.Model;
            viewModel.Products.Count.ShouldEqual(1);
            viewModel.Products[0].Name.ShouldEqual("Neptune");
            viewModel.CurrentCategory.ShouldEqual("Roman");
        }
    }
}
