﻿namespace SportsStore.Tests
{
    using System.Linq;
    using Core.Contracts;
    using Core.Models.Entities;
    using Core.Models.ViewModels;
    using Moq;
    using NUnit.Framework;
    using Ui.Controllers;

    [TestFixture]
    public class ShoppingBasketTests
    {
        [Test]
        public void BasketCanBeCleared()
        {
            var basket = new Basket();
            basket.AddItem(new Product(), 1);

            basket.Clear();
            basket.Lines.Count.ShouldEqual(0);
        }

        [Test]
        public void BasketCombinesLinesWithSameProduct()
        {
            // Arrange - given we have two products
            var p1 = new Product { ProductId = 1 };
            var p2 = new Product { ProductId = 2 };

            // Act - when we add them to a basket multiple times
            var basket = new Basket();
            basket.AddItem(p1, 1);
            basket.AddItem(p1, 2);
            basket.AddItem(p2, 10);

            // Assert - then lines combine quantities for distinct products.
            basket.Lines.Count.ShouldEqual(2);
            basket.Lines.First(x => x.Product.ProductId == 1).Quantity.ShouldEqual(3);
            basket.Lines.First(x => x.Product.ProductId == 2).Quantity.ShouldEqual(10);
        }

        [Test]
        public void BasketStartsEmpty()
        {
            new Basket().Lines.Count.ShouldEqual(0);
        }

        [Test]
        public void BasketTotalValueIsSumOfPriceTimesQuantity()
        {
            var basket = new Basket();
            basket.AddItem(new Product { ProductId = 1, Price = 5 }, 10);
            basket.AddItem(new Product { ProductId = 2, Price = 2.1M }, 3);
            basket.AddItem(new Product { ProductId = 3, Price = 1000 }, 1);

            basket.CalculateTotalValue().ShouldEqual(1056.3M);
        }

        [Test]
        public void CanRemoveItemFromBasket()
        {
            // Arrange - given a basket containing two products
            var p1 = new Product { ProductId = 1 };
            var p2 = new Product { ProductId = 2 };

            var basket = new Basket();
            basket.AddItem(p1, 3);
            basket.AddItem(p2, 1);

            // Act - when a product is removed
            basket.RemoveLine(p1);

            // Assert - the basket should not contain a reference to that item
            basket.Lines.Count.ShouldEqual(1);
            Assert.IsNull(basket.Lines.FirstOrDefault(x => x.Product.ProductId == p1.ProductId));
        }

        [Test]
        public void CanAddProductToBasket()
        {
            // Arrange - given a repository with some products
            var mockProductsRepository =
                TestHelpers.MockProductRepository(new Product { ProductId = 123 }, new Product { ProductId = 456 });
            var basketController = new BasketController(mockProductsRepository, null);
            var basket = new Basket();

            // Act - when a user adds a product to their basket
            basketController.AddToBasket(basket, 456, null);

            // Assert - then the product is added to their basket
            basket.Lines.Count.ShouldEqual(1);
            basket.Lines[0].Product.ProductId.ShouldEqual(456);
        }

        [Test]
        public void AfterAddingProductToBasketUserGoesToYourBasketScreen()
        {
            // Arrange - given a repository with some products
            var mockProductRepository = TestHelpers.MockProductRepository(new Product { ProductId = 1 });
            var basketController = new BasketController(mockProductRepository, null);
            
            // Act - when a user adds a product to their basket
            var result = basketController.AddToBasket(new Basket(), 1, "someReturnUrl");

            // Assert - then the user is redirected to the basket index page
            result.ShouldBeRedirectTo(new { action = "Index", returnUrl = "someReturnUrl" });
        }

        [Test]
        public void CanRemoveProductFromBasket()
        {
            // Arrange - given a repository with some products
            var product = new Product { ProductId = 1 };
            var mockProductRepository = TestHelpers.MockProductRepository(product);
            var basketController = new BasketController(mockProductRepository, null);

            // and a basket containing a product
            var basket = new Basket();
            basket.AddItem(product, 3);

            // Act - when a user removes an item from their basket
            basketController.RemoveFromBasket(basket, 1, null);

            // Assert - then the basket should be empty
            basket.Lines.Count.ShouldEqual(0);
        }

        [Test]
        public void CanViewBasketContents()
        {
            // Arrange/act - given the user visits BasketControllers index action
            var basket = new Basket();
            var result = new BasketController(null, null).Index(basket, "someReturnUrl");

            // Assert - then the view has their basket and the correct return URL
            var viewModel = (BasketIndexViewModel) result.ViewData.Model;
            viewModel.Basket.ShouldEqual(basket);
            viewModel.ReturnUrl.ShouldEqual("someReturnUrl");
        }

        [Test]
        public void CanCheckOutIfBasketIsEmpty()
        {
            // Arrange/act - when the user tries to check out with an empty basket
            var basket = new Basket();
            var shippingDetails = new ShippingDetails();
            var result = new BasketController(null, null).CheckOut(basket, shippingDetails);

            // Assert
            result.ShouldBeDefaultView();
        }

        [Test]
        public void CannotCheckoutIfShippingDetailsAreInvalid()
        {
            // Arrange - given a non empty basket
            var basket = new Basket();
            basket.AddItem(new Product(), 1);

            // Arrange - given invalid shipping details
            var basketController = new BasketController(null, null);
            basketController.ModelState.AddModelError("key", "error");

            // Act - when the user tries to check out
            var result = basketController.CheckOut(basket, new ShippingDetails());

            // Assert
            result.ShouldBeDefaultView();
        }

        [Test]
        public void CanCheckoutAndSubmitOrder()
        {
            var mockOrderSubmitter=  new Mock<IOrderSubmitter>();

            // Arrange - given a non-empty basket and no validation errors
            var basket = new Basket();
            basket.AddItem(new Product(), 1);
            var shippingDetails = new ShippingDetails();

            // Act - when the user tries to check out
            var basketController = new BasketController(null, mockOrderSubmitter.Object);
            var result = basketController.CheckOut(basket, shippingDetails);

            // Assert - order goes to the order submitter and user sees "completed" view
            mockOrderSubmitter.Verify(x => x.SubmitOrder(basket, shippingDetails));
            result.ShouldBeView("Completed");
        }

        [Test]
        public void AfterCheckingOutBasketIsEmptied()
        {
            // Arrange/act - Given a valid order submission
            var basket = new Basket();
            basket.AddItem(new Product(), 1);
            new BasketController(null, new Mock<IOrderSubmitter>().Object).CheckOut(basket, new ShippingDetails());

            // Assert - the basket is emptied
            basket.Lines.Count.ShouldEqual(0);
        }
    }
}
