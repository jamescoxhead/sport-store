﻿using System;
using System.Web.Mvc;
using NUnit.Framework;
using SportsStore.Core.ExtensionMethods;
using SportsStore.Core.Models.Entities;
using SportsStore.Core.Models.ViewModels;
using SportsStore.Ui.Controllers;

namespace SportsStore.Tests
{
    [TestFixture]
    public class PaginationTests
    {
        [Test]
        public void CanGenerateLinksToOtherPages()
        {
            // Arrange
            HtmlHelper html = null;

            var pagingInfo = new PagingInfo
            {
                CurrentPage = 2,
                TotalItems = 28,
                ItemsPerPage = 10
            };
            Func<int, string> pageUrl = i => "Page" + i;

            // Act
            var result = html.PageLinks(pagingInfo, pageUrl);

            // Assert
            result.ToString().ShouldEqual(@"<a href=""Page1"">1</a><a class=""selected"" href=""Page2"">2</a><a href=""Page3"">3</a>");
        }

        [Test]
        public void ProductsListIncludesCorrectPageNumbers()
        {
            // Arrange - given 5 products in the repository
            var repository = TestHelpers.MockProductRepository(new Product { Name = "P1" }, new Product { Name = "P2" },
                                                               new Product { Name = "P3" }, new Product { Name = "P4" },
                                                               new Product { Name = "P5" });
            var controller = new ProductsController(repository)
            {
                PageSize = 3
            };

            // Act - when the user asks for the second page
            var result = controller.List(null, 2);

            // Assert - they'll see page links matching the following
            var viewModel = (ProductsListViewModel) result.ViewData.Model;
            PagingInfo pagingInfo = viewModel.PagingInfo;
            pagingInfo.CurrentPage.ShouldEqual(2);
            pagingInfo.ItemsPerPage.ShouldEqual(3);
            pagingInfo.TotalItems.ShouldEqual(5);
            pagingInfo.TotalPages.ShouldEqual(2);
        }
    }
}
