﻿namespace SportsStore.Tests
{
    using System.Linq;
    using System.Runtime.CompilerServices;
    using System.Web.Mvc;
    using System.Web.Routing;
    using Core.Contracts;
    using Core.Models.Entities;
    using Moq;
    using NUnit.Framework;

    public static class TestHelpers
    {
        public static void ShouldEqual<T>(this T actualValue, T expectedValue)
        {
            Assert.AreEqual(expectedValue, actualValue);
        }

        public static IProductRepository MockProductRepository(params Product[] products)
        {
            var mockRepository = new Mock<IProductRepository>();
            mockRepository.Setup(x => x.Products).Returns(products.AsQueryable());

            return mockRepository.Object;
        }

        public static void ShouldBeRedirectTo(this ActionResult actionResult, object expectedRouteValues)
        {
            var actualValues = ((RedirectToRouteResult) actionResult).RouteValues;
            var expectedValues = new RouteValueDictionary(expectedRouteValues);

            foreach (var key in expectedValues.Keys)
            {
                actualValues[key].ShouldEqual(expectedValues[key]);
            }
        }

        public static void ShouldBeDefaultView(this ActionResult actionResult)
        {
            actionResult.ShouldBeView(string.Empty);
        }

        public static void ShouldBeView(this ActionResult actionResult, string viewName)
        {
            Assert.IsInstanceOf<ViewResult>(actionResult);
            ((ViewResult) actionResult).ViewName.ShouldEqual(viewName);
        }
    }
}
