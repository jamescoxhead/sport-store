﻿namespace SportsStore.Tests
{
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using Core.Models.Entities;
    using Core.Models.ViewModels;
    using NUnit.Framework;
    using Ui.Controllers;

    [TestFixture]
    public class NavigationTests
    {
        [Test]
        public void NavMenuIncludesAlphabeticalListOfDistinctCategories()
        {
            // Arrange - given 4 products in 3 categories
            var mockProductsRepository = TestHelpers.MockProductRepository(
                new Product{Category = "Vegetable", Name = "ProductA"},
                new Product{Category = "Animal", Name = "ProductB"},
                new Product{Category = "Vegetable", Name = "ProductC"},
                new Product{Category = "Mineral", Name = "ProductC"});

            // Act - when we render the navigation menu
            var result = new NavigationController(mockProductsRepository).Menu(null);

            // Assert - then the link to categories are distinct categories in alphabetical order
            // and contain enough information to link to that category.
            var categoryLinks = ((IEnumerable<NavigationLink>) result.ViewData.Model)
                .Where(x => x.RouteValues["category"] != null).ToList();

            CollectionAssert.AreEqual(new[] { "Animal", "Mineral", "Vegetable" },
                                      categoryLinks.Select(x => x.RouteValues["category"]));

            foreach (var link in categoryLinks)
            {
                link.RouteValues["controller"].ShouldEqual("Products");
                link.RouteValues["action"].ShouldEqual("List");
                link.RouteValues["page"].ShouldEqual(1);
                link.Text.ShouldEqual(link.RouteValues["category"]);
            }
        }

        [Test]
        public void NavMenuShowsHomeLinkAtTop()
        {
            // Arrange - given any repository
            var mockProductsRepository = TestHelpers.MockProductRepository();

            // Act - when we render the navigation menu
            var result = new NavigationController(mockProductsRepository).Menu(null);

            // Assert - then the top link is to Home
            var topLink = ((IEnumerable<NavigationLink>) result.ViewData.Model).First();
            topLink.RouteValues["controller"].ShouldEqual("Products");
            topLink.RouteValues["action"].ShouldEqual("List");
            topLink.RouteValues["page"].ShouldEqual(1);
            topLink.RouteValues["category"].ShouldEqual(null);
            topLink.Text.ShouldEqual("Home");
        }

        [Test]
        public void NavMenuHighlightsCurrentCategory()
        {
            // Arrange - given two categories
            var mockProductsRepository = TestHelpers.MockProductRepository(new Product
                                                                           {
                                                                               Category = "Vegetable", Name = "ProductA"
                                                                           },
                                                                           new Product
                                                                           {
                                                                               Category = "Animal", Name = "ProductB"
                                                                           }
                                                                          );

            // Act - when we render the navigation menu
            var result = new NavigationController(mockProductsRepository).Menu("Animal");

            // Assert - then only the current category is highlighted
            var highlightedLinks = ((IEnumerable<NavigationLink>) result.ViewData.Model)
                .Where(x => x.IsSelected).ToList();
            highlightedLinks.Count.ShouldEqual(1);
            highlightedLinks[0].Text.ShouldEqual("Animal");
        }
    }
}
