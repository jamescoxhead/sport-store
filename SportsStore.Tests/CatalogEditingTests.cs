﻿namespace SportsStore.Tests
{
    using Core.Models.Entities;
    using NUnit.Framework;
    using Ui.Controllers;

    [TestFixture]
    public class CatalogEditingTests
    {
        [Test]
        public void CanEditASingleProduct()
        {
            // Arrange - given a collection of products
            var repository = TestHelpers.MockProductRepository(new Product { ProductId = 1 },
                                                               new Product { ProductId = 2 },
                                                               new Product { ProductId = 3 });

            // Act - when the user edits a product
            var controller = new AdminController(repository);
            var result = controller.Edit(2);

            // Assert - the result should be the default view and the corresponding product should be returned.
            result.ShouldBeDefaultView();
            ((Product) result.ViewData.Model).ProductId.ShouldEqual(2);
        }
    }
}
