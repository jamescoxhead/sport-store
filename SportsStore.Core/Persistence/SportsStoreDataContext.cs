﻿using System.Data.Entity;
using SportsStore.Core.Models.Entities;

namespace SportsStore.Core.Persistence
{
    public class SportsStoreDataContext : DbContext
    {
        public SportsStoreDataContext() : base("SportsStoreDataContext")
        {
        }

        public DbSet<Product> Products { get; set; }
    }
}
