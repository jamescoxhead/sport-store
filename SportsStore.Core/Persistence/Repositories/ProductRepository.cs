﻿using System.Linq;
using SportsStore.Core.Contracts;
using SportsStore.Core.Models.Entities;

namespace SportsStore.Core.Persistence.Repositories
{
    public class ProductRepository : IProductRepository
    {
        public ProductRepository()
        {
            Products = new SportsStoreDataContext().Products;
        }

        public IQueryable<Product> Products { get; }
    }
}
