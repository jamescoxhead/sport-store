﻿using System.Collections.Generic;
using System.Linq;
using SportsStore.Core.Contracts;
using SportsStore.Core.Models.Entities;

namespace SportsStore.Core.Persistence.Repositories
{
    public class FakeProductRepository : IProductRepository
    {
        private static readonly IQueryable<Product> _products = new List<Product>
        {
            new Product { Name = "Football", Price = 25 },
            new Product { Name = "Surf board", Price = 179 },
            new Product { Name = "Running shoes", Price = 95 }
        }.AsQueryable();

        public IQueryable<Product> Products => _products;
    }
}
