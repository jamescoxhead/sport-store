﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SportsStore.Core.Models.Entities;

namespace SportsStore.Core.Persistence
{
    public class SportsStoreDataInitializer : DropCreateDatabaseIfModelChanges<SportsStoreDataContext>
    {
        protected override void Seed(SportsStoreDataContext context)
        {
            var products = new List<Product>
            {
                new Product
                {
                    Name = "Kayak",
                    Description = "A boat for one person",
                    Category = "Watersports",
                    Price = 275
                },
                new Product
                {
                    Name = "Lifejacket",
                    Description = "Protective and fashionable",
                    Category = "Watersports",
                    Price = 48.95M
                },
                new Product
                {
                    Name = "Soccer ball",
                    Description = "FIFA-approved size and weight",
                    Category = "Soccer",
                    Price = 19.5M
                },
                new Product
                {
                    Name = "Corner flags",
                    Description = "Give your playing field a professional touch",
                    Category = "Soccer",
                    Price = 34.95M
                },
                new Product
                {
                    Name = "Stadium",
                    Description = "Flat-packed 35,000 seat stadium",
                    Category = "Soccer",
                    Price = 79500
                },
                new Product
                {
                    Name = "Thinking cap",
                    Description = "Improve your brain efficiency by 75%",
                    Category = "Chess",
                    Price = 16
                },
                new Product
                {
                    Name = "Unsteady chair",
                    Description = "Secretly give your opponent a disadvantage",
                    Category = "Chess",
                    Price = 29.95M
                },
                new Product
                {
                    Name = "Human chess board",
                    Description = "A fun game for the whole extended family",
                    Category = "Chess",
                    Price = 75
                },
                new Product
                {
                    Name = "Bling-bling king",
                    Description = "Gold-plated, diamond-studded king",
                    Category = "Chess",
                    Price = 1200
                }
            };

            //using (context)
            //{
                products.ForEach(product => context.Products.Add(product));
                context.SaveChanges();
            //}
        }
    }
}
