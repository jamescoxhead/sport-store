﻿using System.Linq;
using SportsStore.Core.Models.Entities;

namespace SportsStore.Core.Contracts
{
    public interface IProductRepository
    {
        IQueryable<Product> Products { get; }
    }
}
