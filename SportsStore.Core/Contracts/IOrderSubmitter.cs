﻿namespace SportsStore.Core.Contracts
{
    using Models.Entities;

    public interface IOrderSubmitter
    {
        void SubmitOrder(Basket basket, ShippingDetails shippingDetails);
    }
}
