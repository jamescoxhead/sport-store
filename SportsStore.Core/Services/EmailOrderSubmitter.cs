﻿namespace SportsStore.Core.Services
{
    using System.Net.Mail;
    using System.Text;
    using Contracts;
    using Models.Entities;

    public class EmailOrderSubmitter : IOrderSubmitter
    {
        private readonly string _mailTo;

        public EmailOrderSubmitter(string mailTo)
        {
            _mailTo = mailTo;
        }

        public void SubmitOrder(Basket basket, ShippingDetails shippingDetails)
        {
            using (var smtpClient = new SmtpClient())
            using (var mailMessage = BuildMailMessage(basket, shippingDetails))
            {
                smtpClient.Send(mailMessage);
            }
        }

        private MailMessage BuildMailMessage(Basket basket, ShippingDetails shippingDetails)
        {
            var body = new StringBuilder();
            body.AppendLine("A new order has been submitted");
            body.AppendLine("---");
            body.AppendLine("Items:");

            foreach (var line in basket.Lines)
            {
                var subtotal = line.Product.Price * line.Quantity;
                body.AppendFormat("{0} x {1} (subtotal: {2:c}", line.Quantity, line.Product.Name, subtotal);
            }

            body.AppendFormat("Total order value: {0:c}", basket.CalculateTotalValue());
            body.AppendLine("---");
            body.AppendLine("Ship to:");
            body.AppendLine(shippingDetails.Name);
            body.AppendLine(shippingDetails.AddressLine1);
            body.AppendLine(shippingDetails.AddressLine2 ?? string.Empty);
            body.AppendLine(shippingDetails.AddressLine3 ?? string.Empty);
            body.AppendLine(shippingDetails.City);
            body.AppendLine(shippingDetails.County ?? string.Empty);
            body.AppendLine(shippingDetails.Postcode);
            body.AppendLine(shippingDetails.Country);
            body.AppendLine("---");
            body.AppendFormat("Gift wrap: {0}", shippingDetails.GiftWrap ? "Yes" : "No");

            return new MailMessage("sportsstore@example.com", _mailTo, "New order submitted", body.ToString());
        }
    }
}
