﻿using System;
using System.Text;
using System.Web.Mvc;
using SportsStore.Core.Models.ViewModels;

namespace SportsStore.Core.ExtensionMethods
{
    public static class HtmlHelperExtensions
    {
        public static MvcHtmlString PageLinks(this HtmlHelper html, PagingInfo pagingInfo, Func<int, string> pageUrl)
        {
            var result = new StringBuilder();

            for (var i = 1; i <= pagingInfo.TotalPages; i++)
            {
                var tag = new TagBuilder("a");
                tag.MergeAttribute("href", pageUrl(i));
                tag.InnerHtml = i.ToString();
                if (i == pagingInfo.CurrentPage)
                    tag.AddCssClass("selected");

                result.Append(tag);
            }

            return MvcHtmlString.Create(result.ToString());
        }

        public static MvcHtmlString FormattedPageLinks(this HtmlHelper html, PagingInfo pagingInfo,
                                                       Func<int, string> pageUrl)
        {
            var resultList = new TagBuilder("ul");
            resultList.AddCssClass("pagination");

            for (int i = 1; i <= pagingInfo.TotalPages; i++)
            {
                var listItem = new TagBuilder("li");
                var listLink = new TagBuilder("a");
                listLink.MergeAttribute("href", pageUrl(i));
                listLink.InnerHtml = i.ToString();

                listItem.InnerHtml = listLink.ToString();

                if (i == pagingInfo.CurrentPage)
                {
                    listItem.AddCssClass("active");
                }

                resultList.InnerHtml = resultList.InnerHtml + listItem.ToString();
            }

            return MvcHtmlString.Create(resultList.ToString());
        }
    }
}
