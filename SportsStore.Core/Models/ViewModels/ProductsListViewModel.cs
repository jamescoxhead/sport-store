﻿using System.Collections.Generic;
using SportsStore.Core.Models.Entities;

namespace SportsStore.Core.Models.ViewModels
{
    public class ProductsListViewModel
    {
        public IList<Product> Products { get; set; }

        public PagingInfo PagingInfo { get; set; }

        public string CurrentCategory { get; set; }
    }
}
