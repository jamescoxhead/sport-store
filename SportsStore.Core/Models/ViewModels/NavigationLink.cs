﻿namespace SportsStore.Core.Models.ViewModels
{
    using System.Web.Routing;

    public class NavigationLink
    {
        public string Text { get; set; }

        public RouteValueDictionary RouteValues { get; set; }

        public bool IsSelected { get; set; }
    }
}
