﻿namespace SportsStore.Core.Models.ViewModels
{
    using Entities;

    public class BasketIndexViewModel
    {
        public Basket Basket { get; set; }

        public string ReturnUrl { get; set; }
    }
}
