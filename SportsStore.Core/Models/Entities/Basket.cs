﻿namespace SportsStore.Core.Models.Entities
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public class Basket
    {
        private readonly List<BasketLine> _lines = new List<BasketLine>();
        public IList<BasketLine> Lines => _lines.AsReadOnly();

        public void AddItem(Product product, int quantity)
        {
            var line = _lines.FirstOrDefault(l => l.Product.ProductId == product.ProductId);
            if (line == null)
            {
                _lines.Add(new BasketLine { Product = product, Quantity = quantity });
            }
            else
            {
                line.Quantity += quantity;
            }
        }

        public decimal CalculateTotalValue()
        {
            return _lines.Sum(l => l.Product.Price * l.Quantity);
        }

        public void Clear()
        {
            _lines.Clear();
        }

        public void RemoveLine(Product product)
        {
            _lines.RemoveAll(l => l.Product.ProductId == product.ProductId);
        }
    }
}
