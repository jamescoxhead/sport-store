﻿namespace SportsStore.Core.Models.Entities
{
    public class BasketLine
    {
        public Product Product { get; set; }

        public int Quantity { get; set; }
    }
}
