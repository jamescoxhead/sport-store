﻿namespace SportsStore.Core.Models.Entities
{
    using System.ComponentModel.DataAnnotations;

    public class ShippingDetails
    {
        [Required(ErrorMessage = "Please enter a name")]
        public string Name { get; set; }

        [Display(Name = "Address Line 1")]
        [Required(ErrorMessage = "Please enter the first line of your address")]
        public string AddressLine1 { get; set; }

        [Display(Name = "Address Line 2")]
        public string AddressLine2 { get; set; }

        [Display(Name = "Address Line 3")]
        public string AddressLine3 { get; set; }

        [Required(ErrorMessage = "Please enter a city name")]
        public string City { get; set; }

        [Required(ErrorMessage = "Please enter a countu name")]
        public string County { get; set; }

        public string Postcode { get; set; }

        [Required(ErrorMessage = "Please enter a country name")]
        public string Country { get; set; }

        [Display(Name = "Gift wrap")]
        public bool GiftWrap { get; set; }
    }
}
