﻿namespace SportsStore.Core.ModelBinders
{
    using System;
    using System.Web.Mvc;
    using Models.Entities;

    public class BasketModelBinder : IModelBinder
    {
        private const string BasketSessionKey = "_basket";

        public object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            if (bindingContext.Model != null)
            {
                throw new InvalidOperationException("Cannot update instances");
            }

            var basket = (Basket) controllerContext.HttpContext.Session[BasketSessionKey];
            if (basket == null)
            {
                basket = new Basket();
                controllerContext.HttpContext.Session[BasketSessionKey] = basket;
            }

            return basket;
        }
    }
}
