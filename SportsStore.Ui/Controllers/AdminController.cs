﻿namespace SportsStore.Ui.Controllers
{
    using System;
    using System.Linq;
    using System.Web.Mvc;
    using Core.Contracts;

    public class AdminController : Controller
    {
        private IProductRepository _productRepository;

        public AdminController(IProductRepository productRepository)
        {
            _productRepository = productRepository;
        }

        public ActionResult Index()
        {
            return View(_productRepository.Products.ToList());
        }

        public ActionResult Create()
        {
            throw new NotImplementedException();
        }

        [HttpPost]
        public ActionResult Create(FormCollection form)
        {
            throw new NotImplementedException();
        }

        public ActionResult Delete(int id)
        {
            throw new NotImplementedException();
        }

        [HttpPost]
        public ActionResult Delete(int id, FormCollection form)
        {
            throw new NotImplementedException();
        }

        public ViewResult Edit(int productId)
        {
            var product = _productRepository.Products.First(p => p.ProductId == productId);

            return View(product);
        }

        [HttpPost]
        public ActionResult Edit(int productId, FormCollection form)
        {
            throw new NotImplementedException();
        }
    }
}
