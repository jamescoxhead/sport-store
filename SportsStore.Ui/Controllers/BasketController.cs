﻿namespace SportsStore.Ui.Controllers
{
    using System.Linq;
    using System.Web.Mvc;
    using Core.Contracts;
    using Core.Models.Entities;
    using Core.Models.ViewModels;

    public class BasketController : Controller
    {
        private readonly IProductRepository _productRepository;
        private readonly IOrderSubmitter _orderSubmitter;

        public BasketController(IProductRepository productRepository, IOrderSubmitter orderSubmitter)
        {
            _productRepository = productRepository;
            _orderSubmitter = orderSubmitter;
        }

        public ViewResult Index(Basket basket, string returnUrl)
        {
            return View(new BasketIndexViewModel { Basket = basket, ReturnUrl = returnUrl });
        }

        public ActionResult AddToBasket(Basket basket, int productId, string returnUrl)
        {
            var product = _productRepository.Products.FirstOrDefault(p => p.ProductId == productId);
            basket.AddItem(product, 1);

            return RedirectToAction("Index", new { returnUrl });
        }

        public ActionResult RemoveFromBasket(Basket basket, int productId, string returnUrl)
        {
            var product = _productRepository.Products.FirstOrDefault(p => p.ProductId == productId);
            basket.RemoveLine(product);

            return RedirectToAction("Index", new { returnUrl });
        }

        [ChildActionOnly]
        public PartialViewResult Summary(Basket basket)
        {
            return PartialView("BasketSummary", basket);
        }

        public ViewResult CheckOut()
        {
            return View(new ShippingDetails());
        }

        [HttpPost]
        public ViewResult CheckOut(Basket basket, ShippingDetails shippingDetails)
        {
            // Empty baskets can't be checked out
            if (basket.Lines.Count == 0)
            {
                ModelState.AddModelError("Basket", "Sorry, your basket is empty!");
            }

            if (ModelState.IsValid)
            {
                _orderSubmitter.SubmitOrder(basket, shippingDetails);
                basket.Clear();

                return View("Completed");
            }

            return View(shippingDetails);
        }
    }
}
