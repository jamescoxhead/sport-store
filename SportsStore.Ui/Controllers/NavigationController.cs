﻿namespace SportsStore.Ui.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;
    using System.Web.Routing;
    using Core.Contracts;
    using Core.Models.ViewModels;

    public class NavigationController : Controller
    {
        private readonly IProductRepository _productRepository;

        public NavigationController(IProductRepository productRepository)
        {
            _productRepository = productRepository;
        }

        [ChildActionOnly]
        public PartialViewResult Menu(string category)
        {
            Func<string, NavigationLink> makeLink = categoryName => new NavigationLink
            {
                Text = categoryName ?? "Home", RouteValues = new RouteValueDictionary(new
                {
                    controller = "Products", action = "List", category = categoryName, page = 1
                }),
                IsSelected = (category == null && categoryName == null) ||
                             (!string.IsNullOrWhiteSpace(categoryName) && !string.IsNullOrWhiteSpace(category) &&
                              categoryName.ToLower() == category.ToLower())
            };

            // Add "Home" to the start of the list
            var navigationLinks = new List<NavigationLink>
            {
                makeLink(null)
            };

            var categories = _productRepository.Products.Select(product => product.Category);
            foreach (var categoryName in categories.Distinct().OrderBy(c => c))
            {
                navigationLinks.Add(makeLink(categoryName));
            }

            return PartialView(navigationLinks);
        }
    }
}
