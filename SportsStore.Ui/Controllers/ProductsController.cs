﻿namespace SportsStore.Ui.Controllers
{
    using System.Linq;
    using System.Web.Mvc;
    using Core.Contracts;
    using Core.Models.ViewModels;

    public class ProductsController : Controller
    {
        private readonly IProductRepository _productRepository;
        public int PageSize = 4;

        public ProductsController(IProductRepository productRepository)
        {
            _productRepository = productRepository;
        }

        public ViewResult List(string category, int page = 1)
        {
            var productsToShow = string.IsNullOrWhiteSpace(category)
                                     ? _productRepository.Products
                                     : _productRepository.Products.Where(product => product.Category.ToLower() ==
                                                                                    category.ToLower());

            var viewModel = new ProductsListViewModel
            {
                Products = productsToShow.OrderBy(product => product.ProductId).Skip((page - 1) * PageSize)
                                         .Take(PageSize).ToList(),
                PagingInfo = new PagingInfo
                {
                    CurrentPage = page,
                    ItemsPerPage = PageSize,
                    TotalItems = productsToShow.Count()
                },
                CurrentCategory = category
            };

            return View(viewModel);
        }
    }
}
