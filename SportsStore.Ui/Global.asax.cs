﻿using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace SportsStore.Ui
{
    using Core.ModelBinders;
    using Core.Models.Entities;

    public class MvcApplication : HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            AutofacConfig.ConfigureAutofac();

            ModelBinders.Binders.Add(typeof(Basket), new BasketModelBinder());
        }
    }
}
