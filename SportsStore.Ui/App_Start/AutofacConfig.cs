﻿namespace SportsStore.Ui
{
    using System.Reflection;
    using System.Web.Mvc;
    using Autofac;
    using Autofac.Integration.Mvc;
    using Core.Contracts;
    using Core.Persistence.Repositories;
    using Core.Services;

    public class AutofacConfig
    {
        public static void ConfigureAutofac()
        {
            var builder = new ContainerBuilder();

            ConfigureMvc(builder);
            ConfigureServices(builder);

            var container = builder.Build();
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
        }

        private static void ConfigureMvc(ContainerBuilder builder)
        {
            builder.RegisterControllers(Assembly.GetCallingAssembly());
        }

        public static void ConfigureServices(ContainerBuilder builder)
        {
            builder.RegisterType<ProductRepository>().As<IProductRepository>();
            builder.RegisterType<EmailOrderSubmitter>().As<IOrderSubmitter>()
                   .WithParameter("mailTo", "test@example.com");
        }
    }
}
